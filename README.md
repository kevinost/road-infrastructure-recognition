# Road-Infrastructure-Recognition 



## Overview
_What?_  
In this project, an approach was implemented to facilitate the validation process of geodata in OpenStreetMap by a computer-based method. Specifically, a pre-trained neural network was systematically trained to automatically identify pedestrian crossings on orthophotos and to obtain their coordinates. With the help of the extracted coordinates, a reconciliation process, i.e. the matching of the extracted coordinates with the OpenStreetMap data, is facilitated.

_How?_  
The project was implemented using the Fastai library, which is based on the Pytorch Deep-Learning-Framework.  
The applied architecture, i.e. ResNet18, was trained on 5162 orthophotos. In a first step, the network was trained in a frozen state during three epochs. Subsequently, the network was opened (unfreezed) and trained again during 20 epochs.

| Images | Layers | Transform | Freezed | Epoch | Learning Rate | Train_Loss | Valid_Loss | Accuracy | 
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| 5162 | 18 | Yes | Yes | 3 | 0.005 | 0.0206 | 0.0205 | 0.85 |
| 5162 | 18 | Yes | No | 20 | 0.000001-0.0001 | 0.0173 | 0.0185 | 0.87 |

_Result_  
Our model was tested on 1290 unknown orthophotos. The resulting predicted masks were compared to the effective masks. The measurement with the 1290 unknown orthophotos indicates how the model performs on "real world" data and is therefore representative.  
In this project, all three metrics (Precision, Recall, and F1 score) were calculated at both pixel level and object level. 

<img src="img/results.png"  width="300" height="250">  

Comment on the object level:  
Of a total of 2002 pedestrian crossings, the model was able to recognize 1943 correctly. This corresponds to a recall of 97%. About 96% of the detected pedestrian crossings were actually pedestrian crossings (precision).

#### UseCase1: Build your own Model  
Find the code of the entire process here:  
* PreProcessing →  _01_PreProcess_ImageOrganizer.ipynb_
* Model Training →  _02_ModelTraining.ipynb_
* Prediction / Model Testing → _03_ModelTesting.ipynb_
* PostProcessing → _04_PostProcessing.ipynb_  

You want to take up the challenge and train your own pre-trained model?  
This use case shows the whole process from downloading the images, training and testing the model to extracting the coordinates.

<img src="img/built_your_model.PNG"  width="720" height="350">  

#### UseCase2: Use existing Model
The code of the entire process is available in _99_Crossing_Segmentation.ipynb_  

You want to detect the pedestrian crossings on your own orthophotos with our trained model and ultimately get their coordinates in a GeoJSON? This is your use case. 

<img src="img/use_existing_model.PNG"  width="720" height="210">  

## Installation
Regardless of the use case, the easiest way to use the tool is to **clone the repository** and set up a **conda environment**.  
Since the sources are available as notebooks, it is recommended to **use Jupyter Notebook**  

Follow the installation steps below:
1. Install conda (miniconda or anaconda) and Git
2. Clone the GitLab environment   
    * open git bash
    * execute: ```$ git clone git@gitlab.com:kevinost/road-infrastructure-recognition.git``` 
3. Create an environment from the environment.yml file
    * open git bash in the cloned project folder (road-infrastructure-recognition) 
	* execute:  ```$ conda env create -f environment.yml```
4. Activate the environment
	* execute: ```$ conda activate image_segmentation```
5. Start jupyter notebook  
    * execute ```$ jupyter notebook```

## Prerequisites

* Python Moduls:    All listed in environment.yml
* Operating system: Tool is only tested on Linux
* File Format:      Orthophotos as TIFF

Especially for the first use case, where the neural network is trained, it is recommended to have a machine with GPU available.

## Author
Tool is written by [Kevin Ammann](https://gitlab.com/kevinost).

Feedback is welcome.

